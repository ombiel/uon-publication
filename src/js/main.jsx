let React = window.React = require('react');
let reactRender = require('-aek/react/utils/react-render');
let { VBox } = require('-components/layout');
let { AekReactRouter, RouterView } = require('-components/router');
let Container = require('-components/container');

let router = new AekReactRouter();

let IndexPage = require('./pages/index');
let PublicationsPage = require('./pages/publications');
let NotificationsPage = require('./pages/notifications');

const GenericStore = require('./scripts/generic-store');

let Screen = React.createClass({
  componentDidMount:function() {
    // this is required to trigger a rerender on SimpleStore update
    // only works thanks to React's fast virtual DOM; would impact performance otherwise
    GenericStore.on('change', () => {
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    // we also have a parallel implementation in GenericUtils if you want to avoid this
    if(!String.prototype.endsWith) {
      String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }
  },

  componentWillUnmount:function() {
    GenericStore.off('change');
  },

  render:function() {
    return (
      <Container>
        <VBox>
        <RouterView router={router}>
            <IndexPage path="/" />
            <PublicationsPage path="/publications" />
            <NotificationsPage path="/notifications" />
          </RouterView>
        </VBox>
      </Container>
    );
  }
});

reactRender(<Screen />);
