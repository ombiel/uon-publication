const SimpleStore = require('-aek/simple-store');
const request = require('-aek/request');
const Notifier = require('-aek/notifier');
var _ = require("-aek/utils");
const Config = require('../modules/config');

// these can be disabled by commenting them out of the plugins array in the constructor()
const loggerPlugin = require('-aek/simple-store/plugins/logger');
const localStoragePlugin = require('-aek/simple-store/plugins/localstorage');

// i.e. 'aek2-test-project-key', this is defined as a JavaScript string explicitly in the JSON Config object
const storageKey = Config.storageKey;

// this project comes with an implementation of SimpleStore, but this isn't necessary to manage state in React; it's just what we recommend
// https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/simple-store
// NOTE - with localStoragePlugin enabled, any changes to initialState will only be reflected by clearing your browser's localStorage (or by logging out and back in)
class GenericStore extends SimpleStore {
  constructor() {
    let plugins = [];
    // comment out either of these to deactivate them
    plugins.push(loggerPlugin());
    plugins.push(localStoragePlugin(storageKey));

    super({
      initialState: {
        orcid: {
          orcid: null,
          orcidError: null,
          $$$orcidLoading: true
        },
        notifications: {
          notifications: [],
          notificationsError: null,
          $$$notificationsLoading: true
        },
        publications: {
          publications: [],
          publicationsError: null,
          $$$publicationsLoading: true
        }
      },
      plugins: plugins
    });

    // you can modify state values here if you absolutely have to, i.e. if you're checking for something on startup
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchOrcid(ctx, orcid, isError, errorMessage, loading) {
    ctx.dispatch({
      name: 'ORCID',
      error: isError,
      extend: {
        orcid: orcid,
        orcidError: errorMessage,
        $$$orcidLoading: loading
      }
    });
  }

  fetchOrcid() {
    var ctx = this.context({ group: "ORCID", path: "orcid" });
    this.dispatchOrcid(ctx, null, false, null, true);
    //request.get(_.publicPath("/data/orcid.json")).end((err, res) => {
    request.action("get-orcid", { sso: true }).end((err, res) => {
      // console.log(res);
      if (!err && res && res.body !== null && !res.body.error) {
        this.dispatchOrcid(ctx, res.body.orcId, false, null, false);
      } else {
        this.dispatchOrcid(ctx, null, true, res.body, false);
      }
    });
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchNotifications(ctx, notifications, isError, errorMessage, loading) {
    ctx.dispatch({
      name: 'NOTIFICATIONS',
      error: isError,
      extend: {
        notifications: notifications,
        notificationsError: errorMessage,
        $$$notificationsLoading: loading
      }
    });
  }

  fetchNotifications() {
    var ctx = this.context({ group: "NOTIFICATIONS", path: "notifications" });
    this.dispatchNotifications(ctx, null, false, null, true);
    //request.get(_.publicPath("/data/notifications.json")).end((err, res) => {
    request.action("get-notifications", { sso: true }).end((err, res) => {
      // console.log(res);
      if (!err && res && res.body !== null && !res.body.error) {
        this.dispatchNotifications(ctx, res.body, false, null, false);
      } else {
        this.dispatchNotifications(ctx, null, true, res.body, false);
      }
    });
  }

  // you'll need one dispatch and one fetch method for each service, or group of service calls (depending on implementation)
  dispatchPublications(ctx, publications, isError, errorMessage, loading) {
    ctx.dispatch({
      name: 'PUBLICATIONS',
      error: isError,
      extend: {
        publications: publications,
        publicationsError: errorMessage,
        $$$publicationsLoading: loading
      }
    });
  }

  fetchPublications() {
    var ctx = this.context({ group: "PUBLICATIONS", path: "publications" });
    this.dispatchPublications(ctx, null, false, null, true);
    //request.get(_.publicPath("/data/publications.json")).end((err, res) => {
    request.action("get-publications", { sso: true }).end((err, res) => {
      // console.log(res);
      if (!err && res && res.body !== null && !res.body.error) {
        this.dispatchPublications(ctx, res.body, false, null, false);
      } else {
        this.dispatchPublications(ctx, null, true, res.body, false);
      }
    });
  }

  // message takes plaintext only; HTML will be rendered as such
  // more examples of default parameters values
  genericNotifier(title, message, dismissable, level, autoDismiss = 0, clear = true) {
    if (clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      message: message,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

  // children allows you to pass HTML to the Notifier to be displayed
  notifierWithChildren(title, children, dismissable, level, autoDismiss = 0, clear = true) {
    if (clear) {
      Notifier.clear();
    }

    Notifier.add({
      title: title,
      children: children,
      dismissable: dismissable,
      level: level,
      autoDismiss: autoDismiss
    });
  }

}

module.exports = new GenericStore();
