const React = window.React = require('react');
const Page = require('-components/page');
const { BasicSegment, Segment } = require('-components/segment');
const Notifier = require('-aek/notifier');
const { Panel } = require("-components/layout");
const Button = require("@ombiel/aek-lib/react/components/button");
var { InfoMessage } = require("-components/message");
const moment = require("moment");

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const GenericStore = require('../scripts/generic-store');
const Config = require('../modules/config');

let IndexPage = React.createClass({

  componentDidMount: function () {
    GenericStore.fetchPublications();
  },

  componentWillUnmount: function () {
    Notifier.clear();
  },



  render: function () {
    let publicationsData = GenericStore.get('publications.publications');

    let loading = GenericStore.get('publications.$$$publicationsLoading');
    let publications;
    if (!loading && _.isArray(publicationsData)) {
      if (_.isEmpty(publicationsData)) {
        publications =
          <BasicSegment>
            <InfoMessage>
              <p>No publications are currently available</p>
            </InfoMessage>
          </BasicSegment>;
      } else {
        publications =
          publicationsData.map((publication, index) => {
            return (
              <BasicSegment key={"publication-" + index}>
                <p>{publication.authors.map((author) => {
                  return author + ", ";
                })}
                  <u>{publication.title},</u>
                  {publication.publication ? publication.publication + "," : null}
                  {publication.volume ? publication.volume + "/" : null}
                  {publication.pageRef ? publication.pageRef + "," : null}
                  {moment(publication.date).format(" YYYY-MM-DD ")}
                  ({publication.type})
          </p>
              </BasicSegment>
            );
          });
      }
    } else if (!loading && !_.isArray(publicationsData)) {
      publications =
        <BasicSegment>
          <InfoMessage>
            <p>No publications are currently available</p>
          </InfoMessage>
        </BasicSegment>;
    }

    return (
      <Page className="uon-staff-page">
        <Panel flex={1}>
          <div className="head-wrapper" data-flex={0} key="myheader">
            <div className="head-container menu-wrapper">
              <div className="menu-head">
                <div className="menu-title-wrapper">
                  <div className="menu-flex">
                    <div className="menu-title-flex">
                      <h1 className="menu-title">{Config.language.publications.pageName}</h1>
                    </div>
                    <hr className="menu-line" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <BasicSegment>
            <Segment loading={loading}>
              {publications}
            </Segment>
          </BasicSegment>

          <div data-flex={1} key={'footer'}>
            <BasicSegment>
              <Button className="uon-button" fluid link={"campusm://openURL?url=" + Config.publicationsURL}> {Config.language.publications.buttonText}</Button>
            </BasicSegment>
          </div>
        </Panel>
      </Page>
    );
  }
});

module.exports = IndexPage;
