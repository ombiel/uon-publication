const React = window.React = require('react');
const Page = require('-components/page');
const { BannerHeader } = require('-components/header');
const { BasicSegment, Segment } = require('-components/segment');
const Notifier = require('-aek/notifier');
const {CBox, VBox, Panel} = require("-components/layout");
const {Listview,Item} = require("-components/listview");
const Button = require("@ombiel/aek-lib/react/components/button");

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const GenericStore = require('../scripts/generic-store');
const Config = require('../modules/config');

let IndexPage = React.createClass({

  componentDidMount:function() {
    GenericStore.fetchPublications();
  },

  componentWillUnmount:function() {
    Notifier.clear();
  },



  render:function() {
    let theme = Config.theme.primary;

    let notificationsData = GenericStore.get('notifications.notifications');

    let loading = GenericStore.get('notifications.$$$notificationsLoading');

    let notifications = notificationsData ?
    notificationsData.map((notification, index) => {
      return (
        <Listview key={'notification-' + index }>
          <Item href={notification.url}>
            <p> {notification.description} </p>
          </Item>
        </Listview>
      );
    }) :
    null;

    return (
      <Page className="uon-staff-page">
        <Panel>
        <div className="head-wrapper" data-flex={1} key="myheader">
          <div className="head-container menu-wrapper">
            <div className="menu-head">
              <div className="menu-title-wrapper">
                <div className="menu-flex">
                  <div className="menu-title-flex">
                    <h1 className="menu-title">{Config.language.notifications.pageName}</h1>
                  </div>
                  <hr className="menu-line"/>
                </div>
              </div>
            </div>
          </div>
        </div>

          <BasicSegment loading={loading}>
            {notifications}
          </BasicSegment>
        </Panel>
    </Page>
    );
  }
});

module.exports = IndexPage;
