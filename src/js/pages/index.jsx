const React = window.React = require('react');
const Page = require('-components/page');
const { BannerHeader } = require('-components/header');
const { BasicSegment, Segment } = require('-components/segment');
const Notifier = require('-aek/notifier');
var { CBox, VBox } = require("-components/layout");
let { Listview, Item } = require("-components/listview");

// this is our Lodash library, with some additional functionality. See https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.3.1/pages/other-tools/utils
const _ = require('-aek/utils');

const GenericStore = require('../scripts/generic-store');
const Config = require('../modules/config');

let IndexPage = React.createClass({
  componentDidMount: function () {
    GenericStore.fetchOrcid();
    GenericStore.fetchNotifications();
    GenericStore.fetchPublications();
  },

  componentWillUnmount: function () {
    Notifier.clear();
  },

  render: function () {

    let orcid = GenericStore.get('orcid.orcid');
    let notificationsData = GenericStore.get('notifications.notifications');
    let publicationsData = GenericStore.get('publications.publications');

    let orcidLoading = GenericStore.get('orcid.$$$orcidLoading');
    let notificationsLoading = GenericStore.get('notifications.$$$notificationsLoading');
    let publicationsLoading = GenericStore.get('publications.$$$publicationsLoading');

    let orcidID = orcid ?
      <Listview>
        <Item><h3>{Config.language.index.orcidText}{orcid}</h3></Item>
      </Listview> :
      <Listview>
        <Item><h3>{Config.language.index.orcidText}{Config.language.index.noOrcidText}</h3></Item>
      </Listview>;
    let notificationCount;
    let notificationLink;

    if (_.isArray(notificationsData) && notificationsData.length > 0) {
      let count = notificationsData.length;
      notificationLink = "#/notifications";
      notificationCount = <span className="ui negative circular label" style={{ float: "right" }}>{count}</span>;
    } else if (!_.isArray(notificationsData) && !_.isEmpty(notificationsData) && notificationsData.length > 0) {
      let count = 1;
      notificationLink = "#/notifications";
      notificationCount = <span className="ui negative circular label" style={{ float: "right" }}>{count}</span>;
    } else {
      notificationCount = <span className="ui primary circular label" style={{ float: "right" }}>0</span>;
    }




    let notifications = <Listview>
      <Item key="notifications" href={notificationLink}>
        <div>
          <h3 style={{ float: "left", paddingTop: "0.3em" }}>
            {Config.language.index.notificationsText}
          </h3>
          {notificationCount}
        </div>
      </Item>
    </Listview>;
    // <span className="ui circular label" style={{float:"right", backgroundColor:"black", color:"white"}} title={Config.language.index.infoHoverText}>?</span>

    let publications =
      <Listview key="publications">
        <Item href="#/publications">
          <div>
            <h3 style={{ float: "left", paddingTop: "0.3em" }}>{Config.language.index.publicationsText}</h3>

          </div>
        </Item>
      </Listview>;

    return (
      <Page className="uon-staff-page">
        <div className="head-wrapper">
          <div className="head-container menu-wrapper">
            <div className="menu-head">
              <div className="menu-title-wrapper">
                <div className="menu-flex">
                  <div className="menu-title-flex">
                    <h1 className="menu-title">{Config.language.index.pageName}</h1>
                  </div>
                  <hr className="menu-line" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <BasicSegment className="uon-staff-content" loading={orcidLoading}>
          {orcidID}
        </BasicSegment>
        <BasicSegment className="uon-staff-content" loading={notificationsLoading}>
          {notifications}
        </BasicSegment>
        <BasicSegment className="uon-staff-content" loading={publicationsLoading}>
          {publications}
        </BasicSegment>
        <BasicSegment className="uon-staff-content">
          <Listview>
            <a style={{ cursor: "auto" }} href={Config.loginURL}>
              <div
                className="menu-link"
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  flexDirection: "row",
                  alignItems: "center"
                }}>
                <h3 className="menu-button">{Config.language.index.loginText}</h3>
                <img src="https://portal-ap.campusm.exlibrisgroup.com/assets/Newcastle/myUON/AEK-icons/system_uon_blue.png" className="list-item-image" alt="link icon" />
              </div>
            </a>
          </Listview>
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
